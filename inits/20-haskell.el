
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
;;(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
;;(add-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)

(add-hook 'haskell-mode-hook
          (lambda () (local-set-key (kbd "C-<tab>")
                                    'indent-for-tab-command)))
(add-hook 'haskell-mode-hook 'imenu-add-menubar-index)

(require 'speedbar)
(speedbar-add-supported-extension ".hs")

(add-to-list 'load-path (concat config-dir "plugins/hamlet-mode"))
(require 'hamlet-mode)

(add-to-list 'auto-mode-alist '("\\.chs\\'" . haskell-mode))

(when (getenv "GHC_MOD")
  (add-to-list 'load-path (getenv "GHC_MOD"))

  ;; From the ghc-mod instructions
  (autoload 'ghc-init "ghc" nil t)
  (autoload 'ghc-debug "ghc" nil t)
  (add-hook 'haskell-mode-hook (lambda ()
                                 (ghc-init)
                                 (fci-mode)
                                 (company-mode))))
(unless (getenv "GHC_MOD")
  (add-hook 'haskell-mode-hook (lambda ()
                                 ;; (flycheck-mode)
                                 (company-mode)
                                 (interactive-haskell-mode)
                                 (local-set-key (kbd "C-<return>") 'helm-company)))

  (setq
   haskell-compile-cabal-build-command "cd %s && stack build"
   haskell-process-type 'stack-ghci
   haskell-interactive-popup-errors t
   haskell-process-path-ghci "stack"
   haskell-process-args-ghci "ghci")

  (defun haskell-process-trigger-suggestions-ad (orig-fun &rest args)
    (turn-off-haskell-doc)
    (apply orig-fun args)
    (turn-on-haskell-doc))

  (advice-add 'haskell-process-trigger-suggestions
              :around #'haskell-process-trigger-suggestions-ad)

  (add-hook 'flycheck-mode-hook 'flycheck-haskell-configure)
  (eval-after-load 'flycheck
    '(add-hook 'flycheck-mode-hook #'flycheck-haskell-setup)))
