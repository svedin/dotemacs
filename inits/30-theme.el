
(when window-system
  (setq subatomic-more-visible-comment-delimiters t)
  ;; We need to require em-ls to get eshell faces which subatomic requires
  (require 'em-ls)
  (load-theme 'subatomic t)

  (require 'color)
  ;; Make line highlighting closeish to background color
  (let* ((background-color (frame-parameter nil 'background-color))
         (hsl-background (apply 'color-rgb-to-hsl (color-name-to-rgb background-color)))
         (lightness (cadr (cdr hsl-background)))
         (lighten-percent 5)
         (lightness-threshold 0.5))
    ;; (set-face-background 'highline-face
    ;;                      (if (> lightness lightness-threshold)
    ;;                          ;; Make it darker
    ;;                          (color-darken-name background-color lighten-percent)
    ;;                        ;; Make it lighter
    ;;                        (color-lighten-name background-color lighten-percent)))))
    ))

(unless window-system
  (load-theme 'wombat t))
