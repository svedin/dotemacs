; -*- coding: utf-8; -*-

;;; Disable tool bar
(tool-bar-mode -1)

;;; Cursor settings
(blink-cursor-mode t)
(setq-default cursor-type 'bar)

;;; Show columns
(column-number-mode t)

(setq-default fill-column 80)

(global-git-gutter-mode +1)

(pending-delete-mode t)

;; Format for window title
(setq frame-title-format "Emacs: %b")

;; Disable bell
(setq ring-bell-function (lambda ()))

;;; Don't show splash screen
(setq inhibit-splash-screen t)

;;; Empty *scratch*
(setq initial-scratch-message nil)

;;; CamelCase Support
(global-subword-mode t)

;;; Don't indent with tabs
(setq-default indent-tabs-mode nil)

;; Tramp
(eval-after-load 'tramp '(setenv "SHELL" "/bin/bash"))
(setq tramp-default-method "ssh")

;;; Whitespace
(setq-default show-trailing-whitespace t)
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;;; Encoding
(set-language-environment "UTF-8")

;;; W3M
(setq w3m-default-display-inline-images t)
(setq mime-w3m-safe-url-regexp nil)

;;; Parenthesis matching
(show-paren-mode t)

;;; Disable menu when no window present
(unless window-system
  (menu-bar-mode -1))

;; Rainbow mode
(when window-system
  (add-hook 'css-mode-hook (lambda () (rainbow-mode))))


(when (string-match "windows" (symbol-name system-type))
  (set-frame-font
   "Consolas-14"))

;; (set-frame-font "Source Code Pro-14")

;; Make all "yes or no" promts ask "y or n" instead
(fset 'yes-or-no-p 'y-or-n-p)

(add-to-list 'auto-mode-alist '("\\.envrc\\'" . shell-script-mode))

(use-package highlight-parentheses
  :if window-system
  :init
  (setq hl-paren-colors '("firebrick1" "magenta" "SpringGreen2" "coral"))
  :config
  (progn
    (global-highlight-parentheses-mode 1)))

(use-package nyan-mode
  :init
  (progn
    (nyan-mode 1)
    (setq nyan-bar-length 16)))

(use-package align
  :bind ("C-c a" . align-regexp))

(use-package sh-script
  :mode ("\\.zsh\\'" . shell-script-mode))

(use-package files
  :init
  (progn
  (setq
   ;; Backup files handling
   backup-by-copying t
   backup-directory-alist '(("." . "~/.saves")))
   ;; A file has to end with a newline
   (setq-default require-final-newline t)))

;; Display Time
(use-package time
  :init (display-time-mode t)
  :config
  (setq display-time-24hr-format t))

;;; IDO Mode
(use-package ido
  :init (ido-mode 0)
  :config
  (progn
    ;; Don't automaticly search for files
    (setq ido-auto-merge-work-directories-length -1)
    ;; Fuzzy matching
    (setq ido-enable-flex-matching t)
    ;; To open directory in find-file
    (add-to-list 'ido-ignore-buffers "^ ")
    (add-to-list 'ido-ignore-buffers "\\*Buffer List\\*")
    (add-to-list 'ido-ignore-buffers "Completions\\*")))

;; Ace Jump Mode
(use-package ace-jump-mode
  :bind (("C-c C-SPC" . ace-jump-mode)
         ("C-c C-l" . ace-jump-line-mode)))

(use-package jump-char
  :bind ("C-c C-f" . jump-char-forward))

;; Iy Go To Char
(use-package iy-go-to-char
  :bind ("C-c f" . iy-go-to-char))

(use-package magit
  :bind ("C-x g" . magit-status)
  :init (setq magit-diff-refine-hunk t))

;; (use-package magit-files
;;   :config
;;   (global-magit-file-mode))

(use-package dired
  :init
  (progn
    (setq dired-listing-switches "-alhX --group-directories-first")
    ;; (toggle-diredp-find-file-reuse-dir 1)
    (when (and
           (or t (string-match "windows" (symbol-name system-type)))
           (executable-find "ls"))
      ;; Always use ls
      (setq ls-lisp-use-insert-directory-program t)
      (setq insert-directory-program "ls"))))

;; YAML-mode
(use-package yaml-mode
  :mode ("\\.yml$" . yaml-mode))

;; org-mode
(use-package org
  :commands org-mode
  :config
  (require 'org-compat)
  (setq org-startup-indented t)
  (add-to-list 'load-path (concat config-dir "plugins/org-exporters"))
  (require 'ox-confluence)
  (require 'ox-jira)
  (require 'ox-reveal))

(use-package org-bullets
  :defer t
  :init
  (progn
    (add-hook 'org-mode-hook 'org-bullets-mode)
    (setq org-bullets-bullet-list '("■" "◆" "▲" "▶"))))

;; Make sensible names duplicate buffer names
(use-package uniquify
  :init
  (setq uniquify-buffer-name-style 'post-forward))

;; Flymake
(use-package flymake
  :init
  (setq flymake-run-in-place nil))

(use-package browse-kill-ring
  :bind ("C-c k" . browse-kill-ring))

(use-package which-key
  :config (which-key-mode)
  :init
  (which-key-setup-side-window-right-bottom))

;; Easier window navigation
(use-package windmove
  :init
  (windmove-default-keybindings))

;; yasnippet
(use-package yasnippet
  :init
  (progn
    (yas-global-mode 1)
    (setq-default yas-prompt-functions '(yas-ido-prompt yas-dropdown-prompt))))

;; Speedbar
(use-package sr-speedbar
  :bind ("s-b" . sr-speedbar-toggle)
  :init
  (progn
    (setq sr-speedbar-right-side nil)))

(use-package sh-script
  :bind (("M-n" . flycheck-next-error)
         ("M-p" . flycheck-previous-error)
         ("M-l" . flycheck-list-errors))
  :init
  (add-hook 'sh-mode-hook 'flycheck-mode))

;; helm
(defalias 'helm-buffer-match-major-mode 'helm-buffers-list--match-fn)
(use-package helm
  :bind (("C-ö" . helm-imenu)
         ("C-ä" . helm-projectile)
         ("C-å" . helm-swoop)
         ("C-<f1>" . helm-rg)
         ("C-x C-f" . helm-find-files)
         ("C-x b" . helm-buffers-list)
         ("M-x" . helm-M-x)
         )
  :init
  (progn
    (setq helm-ff-DEL-up-one-level-maybe t)
    (setq helm-buffer-max-length 40)
    (setq helm-buffers-fuzzy-matching t)))

; Projectile
(use-package projectile
  :init
  (projectile-global-mode)
  :config
  (setq projectile-indexing-method 'alien))

(use-package proced
  :init
  (add-to-list 'same-window-regexps "\\*Proced\\*")
  (setq proced-tree-flag t))

(use-package graphviz-dot-mode
  :config
  (setq graphviz-dot-auto-indent-on-semi nil))

(defhydra hydra-straight-helper (:hint nil)
  "
_c_heck all       |_f_etch all     |_m_erge all      |_n_ormalize all   |p_u_sh all
_C_heck package   |_F_etch package |_M_erge package  |_N_ormlize package|p_U_sh package
----------------^^+--------------^^+---------------^^+----------------^^+------------||_q_uit||
_r_ebuild all     |_p_ull all      |_v_ersions freeze|_w_atcher start   |_g_et recipe
_R_ebuild package |_P_ull package  |_V_ersions thaw  |_W_atcher quit    |prun_e_ build"
  ("c" straight-check-all)
  ("C" straight-check-package)
  ("r" straight-rebuild-all)
  ("R" straight-rebuild-package)
  ("f" straight-fetch-all)
  ("F" straight-fetch-package)
  ("p" straight-pull-all)
  ("P" straight-pull-package)
  ("m" straight-merge-all)
  ("M" straight-merge-package)
  ("n" straight-normalize-all)
  ("N" straight-normalize-package)
  ("u" straight-push-all)
  ("U" straight-push-package)
  ("v" straight-freeze-versions)
  ("V" straight-thaw-versions)
  ("w" straight-watcher-start)
  ("W" straight-watcher-quit)
  ("g" straight-get-recipe)
  ("e" straight-prune-build)
  ("q" nil))
