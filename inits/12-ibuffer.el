
(global-set-key (kbd "C-x C-b") 'ibuffer)

;; Hide groups that are empty
(setq ibuffer-show-empty-filter-groups nil)

(setq ibuffer-groups
      '(("Org" (mode . org-mode))
        ("Newsticker" (name . "\\*Newsticker .*"))
        ("Shell" (or
                  (mode . shell-mode)
                  (mode . eshell-mode)))
        ("Internal" (or
                     (mode . compilation-mode)
                     (mode . Custom-mode)
                     (mode . apropos-mode)
                     (name . "\\*Messages\\*")
                     (name . "\\*scratch\\*")
                     (name . "\\*Completions\\*")
                     (name . "\\*Packages\\*")
                     (name . ".*helm.*")))
        ("Documentation" (or
                          (mode . Man-mode)
                          (mode . info-mode)
                          (mode . woman-mode)
                          (mode . help-mode)))
        ("Wanderlust" (or
                       (mode . wl-folder-mode)
                       (mode . wl-summary-mode)
                       (mode . mime-view-mode)))
        ("Magit" (name . "\\*magit"))
        ("Twitter" (mode . twittering-mode))
        ))

(defun set-ibuffer-filter-groups ()
  "Set the current filter groups to filter by vc root dir."
  (interactive)
  (setq ibuffer-filter-groups
        (append
         ibuffer-groups
         (ibuffer-vc-generate-filter-groups-by-vc-root)
         ))
  (ibuffer-update nil t))

(add-hook 'ibuffer-hook
          (lambda ()
            (set-ibuffer-filter-groups)
            (unless (eq ibuffer-sorting-mode 'alphabetic)
              (ibuffer-do-sort-by-alphabetic))))

;; Human readable sizes
(define-ibuffer-column human-size
  (:name "Size" :inline t)
  (let* ((kibibyte 1024)
         (mebibyte (* kibibyte kibibyte))
         (size (buffer-size)))
    (cond ((> size mebibyte)
           (format "%7.2fMiB" (/ size (float mebibyte))))
          ((> size kibibyte)
           (format "%7.2fKiB" (/ size (float kibibyte))))
          (t (format "%8d" size)))))

(setq ibuffer-formats
      '((mark modified read-only " "
              (name 17 17 :left :elide)
              " "
              (human-size 10 -1 :right)
              " "
              (mode 16 16 :left :elide)
              " "
              filename-and-process)))
