
;; (add-hook 'rust-mode-hook #'racer-mode)
;; (add-hook 'racer-mode-hook #'eldoc-mode)

;; (add-hook 'racer-mode-hook #'company-mode)

;; (add-hook 'rust-mode-hook
;;           (lambda ()
;;             (define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
;;             (define-key rust-mode-map (kbd "C-<return>") 'helm-company))

;; (with-eval-after-load 'rust-mode
;;   (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))

(use-package rust-mode
  :config
  (add-hook 'rust-mode-hook #'eldoc-mode)
  (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)
  (add-hook 'rust-mode-hook 'flycheck-mode)
  :bind (:map rust-mode-map)
        ("M-p" . 'flycheck-previous-error)
        ("M-n" . 'flycheck-next-error)
        ("M-?" . 'flycheck-explain-error-at-point)
        ("TAB" . #'company-indent-or-complete-common)
        ("C-<return>" . 'helm-company))

(use-package racer
  :config
  (add-hook 'rust-mode-hook #'racer-mode)
  (add-hook 'racer-mode-hook #'eldoc-mode)
  (add-hook 'racer-mode-hook #'company-mode)
  ;; :bind (("TAB" . #'company-indent-or-complete-common)
  ;;        ("C-<return>" . 'helm-company))
  )

;; (require 'rust-mode)
;; (define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
;; (setq company-tooltip-align-annotations t)
