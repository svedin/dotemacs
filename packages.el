
(straight-use-package 'use-package)
(straight-use-package '(init-loader
                        :type git
                        :flavor melpa
                        :files ("init-loader.el" "init-loader-pkg.el")
                        :host github :repo "svedin/init-loader"))
(straight-use-package 'esup)
(straight-use-package 'yasnippet)
(straight-use-package 'flycheck)
(straight-use-package 'flymake)
(straight-use-package 'hydra)
(straight-use-package 'key-chord)
(straight-use-package 'auto-sudoedit)
(straight-use-package 'dockerfile-mode)
(straight-use-package 'docker-tramp)
(straight-use-package 'helm-tramp)
(straight-use-package 'typescript-mode)
(straight-use-package 'ox-jira)
(straight-use-package 'ox-reveal)
(straight-use-package 'graphviz-dot-mode)

;; Haskell
(straight-use-package 'haskell-mode)
(straight-use-package 'haskell-snippets)
(straight-use-package 'flycheck-haskell)

;; Rust
(straight-use-package 'rust-mode)
(straight-use-package 'racer)
(straight-use-package 'toml-mode)
(straight-use-package 'flycheck-rust)

;; Org
(straight-use-package 'org-bullets)

;; Groovy
(straight-use-package 'groovy-mode)
(straight-use-package 'gradle-mode)

(straight-use-package 'which-key)
(straight-use-package 'scala-mode)
(straight-use-package 'subatomic-theme)
(straight-use-package 'twittering-mode)
(straight-use-package 'company)
(straight-use-package 'company-ghc)
(straight-use-package 'helm)
(straight-use-package 'helm-company)
(straight-use-package 'helm-swoop)
(straight-use-package 'helm-rg)
(straight-use-package 'projectile)
(straight-use-package 'helm-projectile)
(straight-use-package 'iedit)
(straight-use-package 'ace-jump-mode)
(straight-use-package 'fill-column-indicator)
(straight-use-package 'highlight-parentheses)
(straight-use-package 'browse-kill-ring)
(straight-use-package 'rainbow-mode)
(straight-use-package 'powerline)
(straight-use-package 'yaml-mode)
(straight-use-package 'ibuffer-vc)
(straight-use-package 'magit)
(straight-use-package 'gitconfig-mode)
(straight-use-package 'sr-speedbar)
(straight-use-package 'iy-go-to-char)
(straight-use-package 'web-mode)
(straight-use-package 'git-gutter)
(straight-use-package 'jump-char)
(straight-use-package 'nyan-mode)
