
;;;; Startup optimizations
(setq gc-cons-threshold-original gc-cons-threshold)
(setq gc-cons-threshold (* 1024 1024 100))
(setq file-name-handler-alist-original file-name-handler-alist)
(setq file-name-handler-alist nil)

(defvar bootstrap-version)
(setq straight-check-for-modifications nil)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(defvar config-dir (file-name-directory load-file-name))

(defvar emacsbin (concat config-dir "bin"))
(add-to-list 'exec-path emacsbin)
(add-to-list 'exec-path "~/.cabal/bin")

(load (concat config-dir "packages.el"))

(require 'use-package)

(require 'init-loader)
(setq init-loader-show-log-after-init nil)
(init-loader-load)

;; Customization
(setq custom-file (concat config-dir "custom.el"))
(load-file custom-file)
(put 'downcase-region 'disabled nil)

(run-with-idle-timer
 5 nil
 (lambda ()
   (setq gc-cons-threshold gc-cons-threshold-original)
   (setq file-name-handler-alist file-name-handler-alist-original)
   (makunbound 'gc-cons-threshold-original)
   (makunbound 'file-name-handler-alist-original)
   (message "gc-cons-threshold and file-name-handler-alist restored")))
